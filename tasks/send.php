<?php
require_once __DIR__."/../bootstrap.php";
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$filename = $argv[1];

if (file_exists($filename)) {
	$data = file_get_contents($filename);
	$data = unserialize($data);

	if (is_array($data)) {
		$method = $data["method"];
		$params = $data["params"];

		if ($method) {
			$connection = new AMQPStreamConnection(
				Config::RBMQ_HOST,
				Config::RBMQ_PORT,
				Config::RBMQ_LOGIN,
				Config::RBMQ_PASS
			);

			$channel = $connection->channel();
			$channel->queue_declare(Config::RBMQ_QUEUE, false, false, false, false);

			$text = serialize(array("method" => $method, "params" => $params));
			$msg  = new AMQPMessage($text);
			$channel->basic_publish($msg, '', Config::RBMQ_QUEUE);

			$channel->close();
			$connection->close();
			unlink($filename);
		}
	}
}
?>