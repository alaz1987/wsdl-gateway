<?php
// run script by root user
require_once __DIR__."/../bootstrap.php";
use PhpAmqpLib\Connection\AMQPStreamConnection;

$lock_file     = "/var/run/" . basename($argv[0], ".php") . ".lock";
$lock_dir      = dirname($lock_file);
$delay         = @intval($argv[1]);
$delay         = $delay > 0 ? $delay : 60;
$queue         = Config::RBMQ_QUEUE;
$ws_url        = Config::WS_URL;
$ws_login      = Config::WS_USER;
$ws_pass       = Config::WS_PASSWORD;
$support_email = Config::SUPPORT_EMAIL;

if (@$argv[1] == "--help") {
  die("Usage:\n\nphp ".__FILE__." <delay>\n<delay> - interval queue invoke callbacks (default 60 seconds)\n\n");
}

if (!set_time_limit(0)) {
  die("Coud't set maximum time execution\n");
}

if (!is_writable($lock_dir)) {
  die("Permission denied to $lock_dir. Try run script by root user\n");
}

if (!tryLock($lock_file)) {
    die("Already running...\n");
}

echo "\nScript running with $delay seconds interval.\nFor help use php ".__FILE__." --help\n";

# remove the lock on exit
register_shutdown_function('unlink', $lock_file);

function tryLock($lock_file) {
    if (@symlink("/proc/" . getmypid(), $lock_file) !== FALSE) {
        return true; 
    }

    if (is_link($lock_file) && !is_dir($lock_file)) {
        unlink($lock_file);
        return tryLock($lock_file);
    }

    return false;
}

$connection = new AMQPStreamConnection(
                                      Config::RBMQ_HOST,
                                      Config::RBMQ_PORT,
                                      Config::RBMQ_LOGIN,
                                      Config::RBMQ_PASS
);
$channel    = $connection->channel();

$channel->queue_declare($queue, false, false, false, false);

$callback = function($msg) use ($channel, $queue, $ws_url, $ws_login, $ws_pass, $support_email) {

  $body = $msg->body;
  $data = unserialize($body);

  $method = $data['method'];
  $params = $data['params'];
  $params = get_object_vars($params[0]);

  echo " [x] Received method: $method\nMethod params: \n";
  //print_r(get_object_vars($params));
  var_dump($params);

  try {
    $error   = NULL;
    $options = array('login' => $ws_login, 'password' => $ws_pass);

    $client  = new SoapClient($ws_url, $options);
    $resp    = $client->$method($params);

    echo " [x] Method response:\n";
    var_dump($resp);

  } catch (SoapFault $e) {
    if (strtolower($e->faultcode) == 'http'
        &&
        strtolower($e->faultstring) == 'internal server error'
    ) {
      $channel->queue_declare($queue, false, false, false, false);
      $channel->basic_publish($msg, '', $queue);
    } else {
      $error = "Occur error soap service.\n".
               "faultcode: ".$e->faultcode.
               "faultstring: ".$e->faultstring.
               "file: ".__FILE__.":".__LINE__;
    }
  } catch (Exception $e) {
      $error = "Occur undefined error.\n".
               "code: ".$e->getCode().
               "message: ".$e->getMessage().
               "file: ".__FILE__.":".__LINE__;
  }

  if (!empty($error)) {
    mail($support_email, "soap microservice error", $error);
  }
};

$channel->basic_consume($queue, '', false, true, false, false, $callback);

while(count($channel->callbacks)) {
    $channel->wait();
    sleep($delay);
}

$channel->close();
$connection->close();
exit(0);
?>