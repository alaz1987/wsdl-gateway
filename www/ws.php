<?php
require_once __DIR__."/../bootstrap.php";
$_SERVER["HTTP_HOST"] = empty($_SERVER["HTTP_HOST"]) ? Config::HTTP_HOST : $_SERVER["HTTP_HOST"];
if (isset($_REQUEST['wsdl'])) {
	$cache_file = __DIR__.Config::CACHE_DIR."/".basename(__FILE__, ".php").".xml";
	$ws 		= Config::WS_URL;
	$login      = Config::WS_USER;
	$pass       = Config::WS_PASSWORD;

	$cache = new HttpCache($cache_file, $ws, 3600);
	$ws = "http://".$_SERVER["HTTP_HOST"]."/".basename(__FILE__);
	
	$cache->setDefinition($ws);
	$cache->setAuth($login, $pass);
	$return = $cache->getXml();

	if (!empty($return)) {
		header('Cache-Control: no-store, no-cache');
		header('Expires: '.date('r'));
		header("Content-Type: text/xml; charset=utf-8");
		print $return;
	}
} else {
	header('Cache-Control: no-store, no-cache');
	header('Expires: '.date('r'));
	header("Content-Type: text/xml; charset=utf-8");
	ini_set("soap.wsdl_cache_enabled", 0);
	$wsdl 	= "http://".$_SERVER["HTTP_HOST"]."/".basename(__FILE__)."?wsdl";
	$server = new SoapServer($wsdl);
	$server->setClass("SoapGateway");
	$server->handle();
}
?>