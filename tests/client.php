<?php
require_once __DIR__."/../bootstrap.php";
header("Content-Type: text/plain; charset=utf-8");
$_SERVER["HTTP_HOST"] = empty($_SERVER["HTTP_HOST"]) ? Config::HTTP_HOST : $_SERVER["HTTP_HOST"];

$wsdl 	  = empty($argv[1]) ? "http://".$_SERVER["HTTP_HOST"]."/ws.php?wsdl" : $argv[1];
$options  = array(
				'login'    => Config::WS_USER,
				'password' => Config::WS_PASSWORD,
				'trace'    => true
);
$client   = new SoapClient($wsdl, $options);

$filename = "files/car.jpg";
$handle   = fopen($filename, "r");
$contents = fread($handle, filesize($filename));
fclose($handle);

$methods = array(
	/* test methods */	
	"GetRealTime" => array(),

	/* sync methods */

	"SaveFileTest" => array(
		"ID" 		=> "1237899",
		"File" 		=> $contents,
		"Extension" => pathinfo($filename, PATHINFO_EXTENSION),
		"Type" 		=> "Test String",
		"Name" 		=> pathinfo($filename, PATHINFO_FILENAME)
	),
	/*"GetCreditCardInfo" => array(
		"ID" => "3787"
	),*/
	"SetSuperReserv" => array(
		"ID" => "185",
		"VIN" => "XW8ZZZ8K3AG200181",
		"Date" => "2016-06-14T00:00:00",
		"Name" => "Евгений",
		"Phone" => "(911) 038-9279",
		"Cancel" => false
	),
	/*"SetStatusCreditCard" => array(
		"UID" 	 => "",
		"Status" => ""
	),*/

	"CreateCalcInsurance" => array(
		"ID" => "181777",
		"Model" => "5ceac271-1624-11e4-a8ef-002590327b0d",
		"VIN" => "KMHTC61CBGU253425",
		//"Info" =. ""
	),
	/*"GetHashGroupPhotos" => array(
		"UIDs" => "2944d793-43b3-43d1-a16d-a1435c43edae,2944d793-43b3-43d1-a16d-a1435c43edae"
	),

	/*"GetPhotosVarColor" => array(
		"UIDs" => "e252f915-4c34-43cd-8869-894b1ab67d7a",
		"CarOnly" => true
	),*/
	"SetOrderAM" => array(
		"Order" => '{"Order_UID":"25d8487b-348c-11e6-afb4-002590327b0d","Order_VIN":"XW8ZZZ8K3AG200181","Klient_ID":"3815","Order_Price_AM":"3642810.00","Order_Kredit":false,"Order_Nalich":true,"Packets":"","OnlineNumber":"1735"}'
	),
	/*"GetHashPhotosBUAvto" => array(
		"VINs" => ""
	),
	"GetKreditСonditions" => array(),
	"GetTires" => array(),
	"CreateInvoice" => array(
		"ID" => "",
		"Order" => "",
		"SumInvoice" => 78945.55
	),
	"GetPhotosAcc" => array(
		"UIDs" => ""
	),
	"GetStatusClientFiles" => array(
		"ID" => ""
	),*/
	"GetContractPdf" => array(
		"Order" 	=> '{"Order_UID":"e786a790-3e0a-11e6-afb4-002590327b0d","Order_VIN":"SHSRE5870FU201822","Klient_ID":"186","Order_Price_AM":"1605154.00","Order_Kredit":false,"Order_Nalich":true,"Packets":""}',
		"Offer" 	=> true,
		"Agreement" => true,
		"DKP" 		=> true
	),
	/*"AwaitingMethods" => array(),
	"GetCarStatus" => array(
		"VIN" => "SHSRE5770FU201294"
	),*/
	"SetCreditProfile" => array(
		"CreditProfile" => '{"Klient_ID":"3796","Name":"\u042d\u043d\u0442\u043e\u043d","Surname":"\u0417\u0434\u043e\u0440","MiddleName":"\u0410\u043b\u0435\u043a\u0441\u0435\u0438\u0447","MothersMaidenName":"\u041c\u0430\u043c\u0430\u043d\u044f","DateOfBirth":"","PlaceOfBirth":"\u041e\u043c\u0441\u043a","PassportSeries":"","PassportID":"","PassportDateOfIssue":"","PassportCodeDivision":"4534","PassportIssuedBy":"","PastName":"","InternationalPassportSeries":"","InternationalPassportID":"","InternationalPassportDateOfIssue":"","InternationalPassportCodeDivision":"","InternationalPassportIssuedBy":"","DriversLicense":"","DriversLicenseDateOfIssue":"564","DriversLicenseIssuedBy":"","INN":"","CertificatePFR":"1489899461","Income":null,"AddIncome":null,"FamilyIncome":null,"TotalFamilyIncome":null,"Costs":null,"CostsDoc":null,"CostsCredit":null,"CostsCreditFamily":null,"IncomeDoc":null,"Region":"\u041e\u043c\u0441\u043a\u0430\u044f","District":"","City":"","Street":"","House":"","Building":"","\u0410partment":"","DateOfRegistration":"2016-12-02","RegionFact":"1","DistrictFact":"","CityFact":"","StreetFact":"","HouseFact":"","BuildingFact":"","\u0410partmentFact":"","ApartmentType":"\u0421\u043e\u0431\u0441\u0442\u0432\u0435\u043d\u043d\u043e\u0441\u0442\u044c (\u043f\u043e\u043a\u0443\u043f\u043a\u0430, \u0438\u043d\u0432\u0435\u0441\u0442\u0438\u0446.\u0434\u043e\u0433\u043e\u0432\u043e\u0440)","PhoneRegistration":"9119140526","PhoneLocation":"","MobilePhone":"","WorkPhone":"","AdditionalPhone":"","EMail":"","ContactPerson":"","ContactPersonPhone":"","Education":"\u0412\u044b\u0441\u0448\u0435\u0435","University":"","Specialty":"","EmployerType":"\u0427\u0430\u0441\u0442\u043d\u0430\u044f","EmployerName":"\u0430\u043f\u0440","IndustryArray":{"Industry":"\u0411\u0430\u043d\u043a\u0438 \/ \u0424\u0438\u043d\u0430\u043d\u0441\u044b \/ \u041b\u0438\u0437\u0438\u043d\u0433 \/ \u0421\u0442\u0440\u0430\u0445\u043e\u0432\u0430\u043d\u0438\u0435"},"EmployerID":"\u0430\u043f\u0440","CompanyRegion":"","CompanyDistrict":"","CompanyCity":"","CompanyStreet":"\u043f","CompanyHouse":"","CompanyBuilding":"","CompanyOffice":"","Position":"","CompanyPhone":"","WorkDate":"","Experience":"","OverallExperience":"","SumWorker":"","FamilyStatus":"\u0425\u043e\u043b\u043e\u0441\u0442 \/ \u041d\u0435 \u0437\u0430\u043c\u0443\u0436\u0435\u043c","FamilyNumber":"5","ChildNumber":"4","ChildNumber18":"3","SpouseName":"","SpouseSurname":"","SpouseMiddleName":"","SpousDateOfBirth":"","SpousPlaceOfBirth":"","SpousPastName":"","Realty":"","Avto_Marka":"","Avto_Model":"","Avto_ReleaseYear":"","Avto_BuyYear":"","Avto_KASKO":"","Avto_GosNumber":"","Avto_PurchaseMethod":"","AcctNumber":null,"AcctDate":null,"Bank":null,"BankSum":null,"CardType":null}'
	),
	/*"GetCreditProfile" => array(
		"ID" => "3797"
	),*/
	"UpdateСalcInsurance" => array(
		"ID" => "ed760fd7-297a-11e6-8876-002590327b0d",
		"Info" => '{"Klient_ID":"3796","Name":"\u042d\u043d\u0442\u043e\u043d","Surname":"\u0417\u0434\u043e\u0440","MiddleName":"\u0410\u043b\u0435\u043a\u0441\u0435\u0438\u0447","MothersMaidenName":"\u041c\u0430\u043c\u0430\u043d\u044f","DateOfBirth":"","PlaceOfBirth":"\u041e\u043c\u0441\u043a","PassportSeries":"","PassportID":"","PassportDateOfIssue":"","PassportCodeDivision":"4534","PassportIssuedBy":"","PastName":"","InternationalPassportSeries":"","InternationalPassportID":"","InternationalPassportDateOfIssue":"","InternationalPassportCodeDivision":"","InternationalPassportIssuedBy":"","DriversLicense":"","DriversLicenseDateOfIssue":"564","DriversLicenseIssuedBy":"","INN":"","CertificatePFR":"1489899461","Income":null,"AddIncome":null,"FamilyIncome":null,"TotalFamilyIncome":null,"Costs":null,"CostsDoc":null,"CostsCredit":null,"CostsCreditFamily":null,"IncomeDoc":null,"Region":"\u041e\u043c\u0441\u043a\u0430\u044f","District":"","City":"","Street":"","House":"","Building":"","\u0410partment":"","DateOfRegistration":"2016-12-02","RegionFact":"1","DistrictFact":"","CityFact":"","StreetFact":"","HouseFact":"","BuildingFact":"","\u0410partmentFact":"","ApartmentType":"\u0421\u043e\u0431\u0441\u0442\u0432\u0435\u043d\u043d\u043e\u0441\u0442\u044c (\u043f\u043e\u043a\u0443\u043f\u043a\u0430, \u0438\u043d\u0432\u0435\u0441\u0442\u0438\u0446.\u0434\u043e\u0433\u043e\u0432\u043e\u0440)","PhoneRegistration":"9119140526","PhoneLocation":"","MobilePhone":"","WorkPhone":"","AdditionalPhone":"","EMail":"","ContactPerson":"","ContactPersonPhone":"","Education":"\u0412\u044b\u0441\u0448\u0435\u0435","University":"","Specialty":"","EmployerType":"\u0427\u0430\u0441\u0442\u043d\u0430\u044f","EmployerName":"\u0430\u043f\u0440","IndustryArray":{"Industry":"\u0411\u0430\u043d\u043a\u0438 \/ \u0424\u0438\u043d\u0430\u043d\u0441\u044b \/ \u041b\u0438\u0437\u0438\u043d\u0433 \/ \u0421\u0442\u0440\u0430\u0445\u043e\u0432\u0430\u043d\u0438\u0435"},"EmployerID":"\u0430\u043f\u0440","CompanyRegion":"","CompanyDistrict":"","CompanyCity":"","CompanyStreet":"\u043f","CompanyHouse":"","CompanyBuilding":"","CompanyOffice":"","Position":"","CompanyPhone":"","WorkDate":"","Experience":"","OverallExperience":"","SumWorker":"","FamilyStatus":"\u0425\u043e\u043b\u043e\u0441\u0442 \/ \u041d\u0435 \u0437\u0430\u043c\u0443\u0436\u0435\u043c","FamilyNumber":"5","ChildNumber":"4","ChildNumber18":"3","SpouseName":"","SpouseSurname":"","SpouseMiddleName":"","SpousDateOfBirth":"","SpousPlaceOfBirth":"","SpousPastName":"","Realty":"","Avto_Marka":"","Avto_Model":"","Avto_ReleaseYear":"","Avto_BuyYear":"","Avto_KASKO":"","Avto_GosNumber":"","Avto_PurchaseMethod":"","AcctNumber":null,"AcctDate":null,"Bank":null,"BankSum":null,"CardType":null}'
	),
	/*"GetLog" => array(
		"Date" => "2016-06-05T00:00:00"
	),*/
	"CreateInvoiceAll" => array(
		"InvoiceInfo" => '{"Klient_ID":"3795","Invoice_Date":"2016-07-01 18:14:38","Invoice_Var":"\u041f\u043e\u043b\u043d\u0430\u044f \u043e\u043f\u043b\u0430\u0442\u0430 \u0431\u0435\u0437 \u0431\u0440\u043e\u043d\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u044f","Invoice_num":"165","Order_UID":"","Order_Number":"1265","Order_VIN":"Z94CT41DAHR511300","Payment_base":"\u041f\u043e\u043b\u043d\u0430\u044f \u043e\u043f\u043b\u0430\u0442\u0430 \u0431\u0435\u0437 \u0431\u0440\u043e\u043d\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u044f, \u0437\u0430\u043a\u0430\u0437 1265, [Z94CT41DAHR511300] Hyundai Solaris","GoodsInvoice":[{"Good":"vin","Price":605024,"Quantity":1,"Sum":605024,"SumVAT":92291.8}]}'
	),

	/* async methods */
	"CreateFPK"			  => array(
		"ID" 	=> "12378991",
		"Name" 	=> "Test FPK Hyundai Santa Fe",
		"Model" => "241e4b4c-1644-11e4-a8ef-002590327b0d",
		"Phone" => "+7-951-757-77-11"

	),
	"CreateCustomer" => array(
		"ID" 	=> "12378991",
		"Name" 	=> "Максим Сидоров",
		"Phone" => "+7-951-777-77-77"		
	),
	"CreateCreditCard" => array(
		"ID" 		=> "1237899",
		"Name" 		=> "Максим Петров",
		"Model" 	=> "241e4b4c-1644-11e4-a8ef-002590327b0d",
		"Phone" 	=> "+7-911-777-77-77",
		"SumCredit" => 999999.99,
		"TermMonth" => 60,
		"ID_order" 	=> "ПКБ0002488"
	),
	"GetCulcKredit" => array(
		"VIN" 			=> "Z6F4XXEEC4GA14778",
		"Bank_UID" 		=> "52ef5bdf-d26d-4179-a328-6f51b59725fc",
		"Srok" 			=> 60,
		"Price_am" 		=> 966784,
		"First_vznos" 	=> 580070,
		"BU" 			=> false,
		"Flag_SG" 		=> true,
		"KASKO_sum" 	=> 0
	)
);

foreach ($methods as $method => $params) {
	try {
		$resp = $client->$method($params);

		$fileRequest  = __DIR__."/results/{$method}Request.txt";
		$fileResponse = __DIR__."/results/{$method}Response.txt";

		$resultRequest  = "/tests/results/{$method}Request.txt";
		$resultResponse = "/tests/results/{$method}Response.txt";

		ob_start();
		var_dump($params);
		$dump_params = ob_get_clean();

		ob_start();
		var_dump($resp);
		$dump_resp = ob_get_clean();

		if (file_put_contents($fileResponse, $dump_resp) > 0) {
			print ("Method $method invoked - OK\nResult ---> $fileResponse\n\n");
		}
	} catch (Exception $e) {
		$error = $e->getMessage();
		print ("Method $method invoked - FAIL\nError: $error");
		print ("Last request:\n". $client->__getLastRequest() . "\n");
		print ("Last response:\n". $client->__getLastRequest() . "\n\n");
	}
}
?>