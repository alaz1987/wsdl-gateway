<?php
class SoapGateway {
	private $async_methods = array(
		"CreateFPK",
		"CreateCustomer",
		"CreateCreditCard",
		"CreateCalcInsurance"
	);

	function __call($method, $params) {
		if (in_array($method, $this->async_methods)) {
			$return = $this->callQueue($method, $params);
			$return = array("return" => ($return ? "ok" : "error"));
		} else {
			$return = $this->callSoap($method, $params);
		}

		return $return;
	}

	private function callSoap($method, $params) {
		$wsdl 	 = Config::WS_URL;
		$options = array('login' => Config::WS_USER, 'password' => Config::WS_PASSWORD);
		$client  = new SoapClient($wsdl, $options);

		$params = get_object_vars($params[0]);
		$return = $client->$method($params);

		return $return;
	}

	private function callQueue($method, $params) {
		$filename = dirname($_SERVER["DOCUMENT_ROOT"]) . "/tmp/".mktime().".tmp";
		$sender = dirname($_SERVER["DOCUMENT_ROOT"]) . "/tasks/send.php";
		
		$data = serialize(array(
			"method" => $method,
			"params" => $params
		));

		if (file_put_contents($filename, $data)) {
			exec("php $sender $filename", $output, $return);
			return $return === 0;
		}

		return false;
	}
}
?>