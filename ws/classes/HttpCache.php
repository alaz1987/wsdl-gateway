<?php
class HttpCache {
	function __construct($cache_file, $url, $ttl=3600) {
		$this->cache_file = $cache_file;
		$this->url = $url;
		$this->ttl = $ttl;
		$this->timeout = 30;
	}

	function setAuth($login, $pass) {
		$this->login = $login;
		$this->pass = $pass;
	}

	function setTimeout($timeout=30) {
		$this->timeout = $timeout;
	}

	function setDefinition($url) {
		$this->definition = $url;
	}

	function getXml() {
		$return = "";

		if (time() - filemtime($this->cache_file) > $this->ttl) {
			$process = curl_init($this->url);

			if (isset($this->login) && isset($this->pass)) {
				curl_setopt($process, CURLOPT_USERPWD, $this->login . ":" . $this->pass);
			}

			curl_setopt($process, CURLOPT_TIMEOUT, $this->timeout);
			curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($process, CURLOPT_HEADER, FALSE);
			
			$return = curl_exec($process);
			curl_close($process);

			if (!empty($this->definition)) {
				$search = array(
					"/soapbind\:address location=\".+?\"/",
					"/soap12bind\:address location=\".+?\"/"
				);
				$replacement = array(
					"soapbind:address location=\"".$this->definition."\"",
					"soap12bind:address location=\"".$this->definition."\""
				);
				$return = preg_replace($search, $replacement, $return);
			}

			if (empty($return)) {
				$return = file_get_contents($this->cache_file);
			} else {
				file_put_contents($this->cache_file, $return);
			}
		} else {
			$return = file_get_contents($this->cache_file);
		}

		return $return;
	}
}
?>